# Repo for our CS4012 HTML project.

Git is a great tool to be able to code collaboratively, allows us to work on different things in teh same project without fecking it up (too much) for other people.

# LFS (Large File Storage)
Because we will/should have images we are going to need to use LFS.  
Git is good at  tracking text based files but is lackluster when it comes to images/pdf's/word docs/videos.....

Instructions are available [here][git-lfs] (assuming you previously installed git).

Once installed the only thing you may have to do is to double check if the file extension of an image/video is listed in teh ``.gitattributes`` file.  
If it is then everything functions as normal git wise.


[git-lfs]: https://git-lfs.github.com/

# Project structure
Typically, projects are structured in a particular way.  

The first layer (project root) will contain general config files, like this ``readme.md``, ``.gitignore`` and ``.gitattributes`` files.  
It will also often contain a ``src``/``source`` folder where all teh actual code is contained.

If a project has to be compiled it will normally output it to a ``build``/``dest``/``release`` folder.  
This output is not committed to source control (git) so you will find an entry in teh ``.gitignore`` to ignore it in git.

A good project to see this in action would be the [Rust Language][rust] repo


For HTML projects there is another layer, the first file that most browsers will read is the ``index.html`` which would be the page ye see where you go to https://example.com/ whereas you would see a ``page2.html`` as https://example.com/page2  
It is the webserver that displays ``/index.html`` as ``/`` along with other conventions such as stripping the extension (``.html``)



[rust]: https://github.com/rust-lang/rust

# Resources

Some resources to get up and running.

* [Intro to Intro to Programming]
* MDN (Mozilla Developer Network) Documentation
  * [HTML][mdn-html] 
  * [CSS][mdn-css]
  * [JS][mdn-js]



[Intro to Intro to Programming]: https://gitlab.com/c2842/misc/intro-to-intro-to-programming
[mdn-html]: https://developer.mozilla.org/en-US/docs/Web/HTML
[mdn-css]: https://developer.mozilla.org/en-US/docs/Web/CSS
[mdn-js]: https://developer.mozilla.org/en-US/docs/Web/JavaScript